import java.io.*;
import java.util.*;
import java.net.*;

/*
 * Transmit file from client to server
 */
public class Server {
    public static void main(String[] args) {
        try {
            // String serverHost = "127.0.0.1";
            int port = 13579;
            String serverHost = InetAddress.getLocalHost().getHostAddress();

            try {
                for (Enumeration<NetworkInterface> nics = NetworkInterface.getNetworkInterfaces(); nics.hasMoreElements();) {
                    NetworkInterface nic = nics.nextElement();
                    for (Enumeration<InetAddress> addrs = nic.getInetAddresses(); addrs.hasMoreElements();) {
                        InetAddress addr = addrs.nextElement();
                        if (addr.isSiteLocalAddress()) {
                            serverHost = addr.getHostAddress();
                            break;
                        }
                    }
                }
            }
            catch (SocketException ex) {
                System.out.println("Exception while searching for site local address");
                System.out.println(Arrays.toString(ex.getStackTrace()));
            }

            ServerSocket serverSocket = new ServerSocket();
            serverSocket.bind(new InetSocketAddress(serverHost, port));
            System.out.printf ("Server started on %s:%d\n", serverHost, port);
            while (true) {
                Socket socket = serverSocket.accept();
                new Connection(socket);
            }

        }
        catch (UnknownHostException ex) {
            System.out.println("Unknown Host");
            System.out.println(Arrays.toString(ex.getStackTrace()));
        }
        catch (IOException ex) {
            System.out.println("IO Exception");
            System.out.println(Arrays.toString(ex.getStackTrace()));
        }
    }

    private static class Connection {
        public static ArrayList<Socket> connList = new ArrayList<>();
        public Connection (Socket socket){
            connList.add(socket);
            String remoteSock = socket.getRemoteSocketAddress().toString();
            System.out.println(remoteSock + " connected.");
            try {
                BufferedInputStream ins = new BufferedInputStream(socket.getInputStream());
                int fileNameLength = ins.read();
                //System.out.println("FileName Lenght: " + fileNameLength);
                String fileName = new String(ins.readNBytes(fileNameLength));
                System.out.println("FileName: " + fileName);
                int fileSizeLength = ins.read();
                //System.out.printf("FileSizeLength: %d, Long.BYTES: %d\n", fileSizeLength, Long.BYTES);
                String fileSizeStr = new String(ins.readNBytes(fileSizeLength));
                System.out.println("FileSize: " + fileSizeStr);
                long fileSize = Long.parseLong(fileSizeStr);
                File file = new File(new File(System.getProperty("user.home")), fileName);
                FileOutputStream filout = new FileOutputStream(file);
                BufferedOutputStream bufout = new BufferedOutputStream(filout);

                //int ch;
                long transmittedSize = 0;
                int read;
                byte[] buff = new byte[(int)Math.pow(2, 20)];
                while (transmittedSize < fileSize && (read = ins.read(buff)) != -1) {
                    bufout.write(buff, 0, read);
                    transmittedSize += read;
                    System.out.print(getProgressString(fileSize, transmittedSize));
                }
//                while ((ch = ins.read()) != -1 && transmittedSize < fileSize) {
//                    bufout.write(ch);
//                    transmittedSize++;
//                    System.out.print(getProgressString(fileSize, transmittedSize));
//                }
                bufout.flush();
                System.out.printf("\nFile %s saved at %s\n", file.getName(),
                    file.getParentFile());
                socket.close();
            }
            catch (IOException ex) {
                System.out.println("IO Exception");
                System.out.println(Arrays.toString(ex.getStackTrace()));
            }
            System.out.println(remoteSock + " disconnected");
            connList.remove(socket);
        }

        private String getProgressString(long fileSize, long transmitted) {
            StringBuilder progress = new StringBuilder();
            int progressBarLength = 10;
            progress.append('\r');
            progress.append('[');
            for (int i = 0; i < progressBarLength; i++) {
                if (i * fileSize / progressBarLength < transmitted) {
                    progress.append('=');
                }
                else {
                    progress.append(' ');
                }
            }
            progress.append(']');
            if (fileSize > transmitted) {
                progress.append(String.format(" %d%%", (int)((float)transmitted/fileSize*100)));
            }
            else {
                progress.append(" 100% Done!");
            }

            return progress.toString();
        }
    }

}
