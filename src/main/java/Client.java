import java.net.*;
import java.io.*;
import java.util.*;

/*
 * Transmit file from client to server
 */
public class Client {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Usage: java Client serverhost file");
        }
        try {
            int port = 13579;
            String serverHost = args[0];
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(serverHost, port));

            String file = args[1];
            System.out.println("file: " + file);
            File fil = new File(file);
            FileInputStream filin = new FileInputStream(fil);
            BufferedInputStream bufin = new BufferedInputStream(filin);

            BufferedOutputStream socketout = new BufferedOutputStream(socket.getOutputStream());

            // write one integer of file name length in int
            // write file name in bytes
            // write file size length in int
            // write file size in bytes
            // write file in bytes
            String fileName = new File(file).getName();
            socketout.write(fileName.length());
            socketout.write(fileName.getBytes());
            long fileSize = fil.length();
            byte[] fileSizeLength = Long.toString(fileSize).getBytes();
            //System.out.println("fileSizeLength: " + new String(fileSizeLength));
            socketout.write(fileSizeLength.length);
            socketout.write(fileSizeLength);
            //int ch;
            long transmittedSize = 0L;
            int read;
            byte[] buff = new byte[(int)Math.pow(2,20)];
            while ((read = bufin.read(buff)) != -1) {
                socketout.write(buff, 0, read);
                transmittedSize += read;
                System.out.print(getProgressString(fileSize, transmittedSize));
            }
//            while ((ch = bufin.read()) != -1 && transmittedSize < fileSize) {
//                socketout.write(ch);
//                transmittedSize++;
//                System.out.print(getProgressString(fileSize, transmittedSize));
//            }
            socketout.flush();
            System.out.println("\nFile transfer completed.");
            socket.close();
        }
        catch (UnknownHostException ex){
            System.out.println("Unknown Host");
            System.out.println(Arrays.toString(ex.getStackTrace()));
        }
        catch (IOException ex) {
            System.out.println("IO Exception");
            System.out.println(Arrays.toString(ex.getStackTrace()));
        }
    }

    private static String getProgressString(long fileSize, long transmitted) {
        StringBuilder progress = new StringBuilder();
        int progressBarLength = 10;
        progress.append('\r');
        progress.append('[');
        for (int i = 0; i < progressBarLength; i++) {
            if (i * fileSize / progressBarLength < transmitted) {
                progress.append('=');
            }
            else {
                progress.append(' ');
            }
        }
        progress.append(']');
        if (fileSize > transmitted) {
            progress.append(String.format(" %d%%", (int)((float)transmitted/fileSize*100)));
        }
        else {
            progress.append(" 100% Done!");
        }

        return progress.toString();
    }
}
